﻿using System;
using System.Linq;
using System.Text;
using System.Text.Json;
using AirlineSendAgent.Client;
using AirlineSendAgent.Data;
using AirlineSendAgent.Dtos;
using AutoMapper;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace AirlineSendAgent.App
{
    public class AppHost : IAppHost
    {
        private readonly SendAgentDbContext _context;
        private readonly IWebhookClient _client;
        private readonly IMapper _mapper;

        public AppHost(SendAgentDbContext context, IWebhookClient client, IMapper mapper)
        {
            _context = context;
            _client = client;
            _mapper = mapper;
        }
        
        public void Run()
        {
            var factory = new ConnectionFactory
            {
                HostName = "localhost",
                Port = 5672
            };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange:"trigger", type: ExchangeType.Fanout);

            var queueName = channel.QueueDeclare().QueueName;
            channel.QueueBind(queue: queueName, exchange: "trigger", routingKey: "");

            var consumer = new EventingBasicConsumer(channel);
            Console.WriteLine("Listening on the message bus...");

            consumer.Received += async (ModuleHandle, ea) =>
            {
                Console.WriteLine("Event triggered!");
                // convert message body into a string then a Dto
                var body = ea.Body;
                var notificationMessage = Encoding.UTF8.GetString(body.ToArray());
                var message = JsonSerializer.Deserialize<NotificationMessageDto>(notificationMessage);

                var webhookToSend = _mapper.Map<FlightDetailChangePayloadDto>(message);

                // switched to using AutoMapper
                // var webhookToSend = new FlightDetailChangePayloadDto
                // {
                //     WebhookType = message.WebhookType,
                //     WebhookURI = String.Empty,
                //     Secret = String.Empty,
                //     Publisher = String.Empty,
                //     OldPrice = message.OldPrice,
                //     NewPrice = message.NewPrice,
                //     FlightCode = message.FlightCode
                // };

                foreach (var subscription in _context.WebhookSubscriptions.Where(s => s.WebhookType.Equals(message.WebhookType)))
                {
                    webhookToSend.WebhookURI = subscription.WebhookURI;
                    webhookToSend.Secret = subscription.Secret;
                    webhookToSend.Publisher = subscription.WebhookPublisher;

                    await _client.SendWebHook(webhookToSend);
                }
            };

            channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);

            Console.ReadLine();
        }
    }
}