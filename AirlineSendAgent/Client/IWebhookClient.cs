﻿using System.Threading.Tasks;
using AirlineSendAgent.Dtos;

namespace AirlineSendAgent.Client
{
    public interface IWebhookClient
    {
        public Task SendWebHook(FlightDetailChangePayloadDto flightDetailChangePayloadDto);
    }
}