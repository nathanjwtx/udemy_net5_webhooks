﻿using System;

namespace AirlineSendAgent.Dtos
{
    public class FlightDetailChangePayloadDto
    {
        public string Publisher { get; set; } = string.Empty;
        public string Secret { get; set; } = string.Empty;
        public string FlightCode { get; set; }
        public decimal OldPrice { get; set; }
        public decimal NewPrice { get; set; }
        public string WebhookType { get; set; }
        
        // essentially the same Dto as in TravelAgentWeb except for this new property
        public string WebhookURI { get; set; } = string.Empty;
    }
}