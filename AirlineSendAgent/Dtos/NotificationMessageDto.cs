﻿using System;

namespace AirlineSendAgent.Dtos
{
    public class NotificationMessageDto
    {
        // as we're receiving the message in this format from AirlineWeb we can reuse the Dto minus the constructor as
        // we don't want to create a message, just read/receive it
        public string Id { get; set; }
        public string WebhookType { get; set; }
        public string FlightCode { get; set; }
        public decimal OldPrice { get; set; }
        public decimal NewPrice { get; set; }
    }
}
