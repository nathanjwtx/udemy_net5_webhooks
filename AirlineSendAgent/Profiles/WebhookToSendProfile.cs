﻿using AirlineSendAgent.Dtos;
using AutoMapper;

namespace AirlineSendAgent.Profiles
{
    public class WebhookToSendProfile : Profile
    {
        public WebhookToSendProfile()
        {
            CreateMap<NotificationMessageDto, FlightDetailChangePayloadDto>();
        }
    }
}