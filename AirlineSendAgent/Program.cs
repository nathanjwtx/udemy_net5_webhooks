﻿using System;
using System.IO;
using AirlineSendAgent.App;
using AirlineSendAgent.Client;
using AirlineSendAgent.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Npgsql;

namespace AirlineSendAgent
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                // .SetBasePath(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.ToString()) // needed to Debug
                .AddJsonFile("appsettings.json", optional:false, reloadOnChange: true)
                .AddUserSecrets(typeof(Program).Assembly, optional: true)
                .AddEnvironmentVariables()
                .AddCommandLine(args);

            var config = builder.Build();

            var host = Host.CreateDefaultBuilder(args)
                .ConfigureServices((context, services) =>
                {
                    services.AddSingleton<IWebhookClient, WebhookClient>();
                    services.AddSingleton<IAppHost, AppHost>();
                    services.AddDbContext<SendAgentDbContext>(opt =>
                    {
                        var builder = new NpgsqlConnectionStringBuilder(
                                config.GetConnectionString("AirlineWebConnection"));
                        builder.Password = config["dbpassword"];
                        opt.UseNpgsql(builder.ConnectionString);
                    });
                    services.AddHttpClient();
                    services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
                }).Build();
            
            host.Services.GetService<IAppHost>()?.Run();
        }
    }
}