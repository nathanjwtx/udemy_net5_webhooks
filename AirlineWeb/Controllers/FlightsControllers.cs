﻿using System;
using System.Collections.Generic;
using System.Linq;
using AirlineWeb.Data;
using AirlineWeb.Dtos;
using AirlineWeb.MessageBus;
using AirlineWeb.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace AirlineWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightsController : ControllerBase
    {
        private readonly AirlineDbContext _context;
        private readonly IMapper _mapper;
        private readonly IMessageBus _messageBus;

        public FlightsController(AirlineDbContext context, IMapper mapper, IMessageBus messageBus)
        {
            _context = context;
            _mapper = mapper;
            _messageBus = messageBus;
        }

        [HttpGet]
        public ActionResult<FlightDetailReadDto> GetFlights()
        {
            var flights = _context.FlightDetails.ToList();

            var flightsDto = new List<FlightDetailReadDto>();

            foreach (var flightDetail in flights)
            {
                flightsDto.Add(_mapper.Map<FlightDetailReadDto>(flightDetail));
            }

            return Ok(flightsDto);
        }

        [HttpGet("{flightCode}", Name = "GetFlightDetailsByCode")]
        public ActionResult<FlightDetailReadDto> GetFlightDetailsByCode(string flightCode)
        {
            var flight = _context.FlightDetails.FirstOrDefault(
                f => f.FlightCode.ToUpper() == flightCode.ToUpper());

            if (flight == null)
            {
                return NotFound("Couldn't find your flight");
            }

            var flightDto = _mapper.Map<FlightDetailReadDto>(flight);

            return Ok(flightDto);
        }

        [HttpPost]
        public ActionResult<FlightDetailReadDto> CreateFlight(FlightDetailCreateDto createDto)
        {
            var flight = _context.FlightDetails.FirstOrDefault(fc => fc.FlightCode == createDto.FlightCode);

            if (flight == null)
            {
                flight = _mapper.Map<FlightDetail>(createDto);
                try
                {
                    _context.FlightDetails.Add(flight);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }

                var flightReadDto = _mapper.Map<FlightDetailReadDto>(flight);

                return CreatedAtRoute(nameof(GetFlightDetailsByCode),
                    new { flightCode = flight.FlightCode }, flightReadDto);
            }

            return NoContent();
        }

        [HttpPut("{id}")]
        public ActionResult UpdateFlightDetail(int id, FlightDetailUpdateDto flightDetailUpdateDto)
        {
            // check flight exists
            var flight = _context.FlightDetails.FirstOrDefault(f => f.Id == id);

            if (flight == null)
            {
                return NotFound();
            }

            decimal oldPrice = flight.Price;

            _mapper.Map(flightDetailUpdateDto, flight);

            try
            {
                _context.SaveChanges();

                if (oldPrice != flight.Price) // flight.Price after updates applided
                {
                    Console.WriteLine("Price Changed - Place message on bus...");

                    var message = new NotificationMessageDto
                    {
                        WebhookType = "price_change",
                        OldPrice = oldPrice,
                        NewPrice = flight.Price,
                        FlightCode = flight.FlightCode
                    };
                    
                    _messageBus.SendMessage(message);
                }
                else
                {
                    Console.WriteLine("No price change.");
                }

                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}