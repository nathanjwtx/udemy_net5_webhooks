﻿using System;
using System.Linq;
using AirlineWeb.Data;
using AirlineWeb.Dtos;
using AirlineWeb.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace AirlineWeb.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WebhookSubscriptionController : ControllerBase
    {
        private readonly AirlineDbContext _airlineDbContext;
        private readonly IMapper _mapper;

        public WebhookSubscriptionController(AirlineDbContext airlineDbContext, IMapper mapper)
        {
            _airlineDbContext = airlineDbContext;
            _mapper = mapper;
        }

        [HttpGet("{secret}", Name = "GetSubscription")]
        public ActionResult<WebhookSubscriptionReadDto> GetSubscriptionBySecret(string secret)
        {
            var subscription = _airlineDbContext.WebhookSubscriptions.FirstOrDefault(s => s.Secret == secret);

            if (subscription == null)
            {
                return NotFound();
            }
            
            return Ok(_mapper.Map<WebhookSubscriptionReadDto>(subscription));
        }

        [HttpPost]
        public ActionResult<WebhookSubscriptionReadDto> CreateSubscription(WebhookSubscriptionCreateDto webhookSubscriptionCreateDto)
        {
            var subscription =
                _airlineDbContext.WebhookSubscriptions.FirstOrDefault(s =>
                    s.WebhookURI == webhookSubscriptionCreateDto.WebhookURI);

            if (subscription == null)
            {
                subscription = _mapper.Map<WebhookSubscription>(webhookSubscriptionCreateDto);

                subscription.Secret = Guid.NewGuid().ToString();
                subscription.WebhookPublisher = "PanAusAirlines"; // in production this would NOT be hard coded.

                try
                {
                    _airlineDbContext.WebhookSubscriptions.Add(subscription);
                    _airlineDbContext.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return BadRequest(e.Message);
                }

                var webhookSubscriptionReadDto = _mapper.Map<WebhookSubscriptionReadDto>(subscription);
                return CreatedAtRoute("GetSubscription", new { secret = webhookSubscriptionReadDto.Secret },
                    webhookSubscriptionReadDto);
            }
            else
            {
                // optionally could return the updated dto
                return NoContent();
            }
        }
    }
}