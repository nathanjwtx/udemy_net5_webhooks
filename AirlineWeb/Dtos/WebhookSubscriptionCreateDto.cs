﻿using System.ComponentModel.DataAnnotations;

namespace AirlineWeb.Dtos
{
    public class WebhookSubscriptionCreateDto
    {
        [Required]
        public string WebhookURI { get; set; } // from the end user ie travel agent
        [Required]
        public string WebhookType { get; set; }
    }
}