﻿
namespace AirlineWeb.Dtos
{
    public class WebhookSubscriptionReadDto
    {
        public int Id { get; set; }
        public string WebhookURI { get; set; } // from the end user ie travel agent
        public string Secret { get; set; }
        public string WebhookType { get; set; }
        public string WebhookPublisher { get; set; } 
    }
}