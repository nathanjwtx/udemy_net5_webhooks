﻿using System;
using System.Text;
using System.Text.Json;
using AirlineWeb.Dtos;
using RabbitMQ.Client;

namespace AirlineWeb.MessageBus
{
    public class MessageBusClient : IMessageBus
    {
        public void SendMessage(NotificationMessageDto notificationMessage)
        {
            var factory = new ConnectionFactory { HostName = "localhost", Port = 5672 };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();
            channel.ExchangeDeclare(exchange: "trigger", type: ExchangeType.Fanout);

            var msg = JsonSerializer.Serialize(notificationMessage);

            var body = Encoding.UTF8.GetBytes(msg);
                    
            channel.BasicPublish(exchange: "trigger", routingKey: String.Empty, body: body);

            Console.WriteLine("--> Message published on Message Bus");
        }
    }
}
