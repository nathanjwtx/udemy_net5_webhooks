﻿using AutoMapper;

namespace AirlineWeb.Profiles
{
    public class WebhookSubscriptionProfile : Profile
    {
        public WebhookSubscriptionProfile()
        {
            CreateMap<Dtos.WebhookSubscriptionCreateDto, Models.WebhookSubscription>();
            CreateMap<Models.WebhookSubscription, Dtos.WebhookSubscriptionReadDto>();
        }
    }
}