﻿let register = new XMLHttpRequest() // could have used 'fetch' instead

const webhook = {
    "webhookUri": "",
    "webhookType": ""
}

document.querySelector('#alertDanger').setAttribute('hidden', 'true')
document.querySelector('#alertSuccess').setAttribute('hidden', 'true')
document.querySelector('#alertUriMissing').setAttribute('hidden', 'true')

document.querySelector('#register').addEventListener('click', () => {
    let webhookUri = document.querySelector('#webhook').value
    let webhookTypes = document.querySelector('#webhooktype')
    webhook.webhookType = webhookTypes.options[webhookTypes.selectedIndex].text
    webhook.webhookUri = webhookUri
    
    if (webhookUri === null || webhookUri === '') { // this would be something like a regex in a production environment
        document.querySelector('#alertUriMissing').removeAttribute('hidden')
        document.querySelector('#uriMissingMessage').innerHTML = 'Please specify the URI to register the webhook'
        return
    }
    
    const apiUrl = 'https://localhost:5001/api/webhooksubscription'
    register.open("POST", apiUrl, true)
    
    register.setRequestHeader('content-type', 'application/json')
    
    register.onreadystatechange = () => {
        if (register.status === 201) {
            let response = JSON.parse(register.responseText)
            document.querySelector('#successMessage').innerHTML = `Please use webhook secret ${response.secret}`
            document.querySelector('#alertSuccess').removeAttribute('hidden')
        } else {
            document.querySelector('#alertDanger').removeAttribute('hidden')
        }
    }
    register.send(JSON.stringify(webhook))
})