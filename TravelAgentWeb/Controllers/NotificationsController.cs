﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TravelAgentWeb.Data;
using TravelAgentWeb.Dtos;

namespace TravelAgentWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotificationsController : ControllerBase
    {
        private readonly TravelAgentDbContext _context;

        public NotificationsController(TravelAgentDbContext context)
        {
            _context = context;
        }

        [HttpPost]
        public ActionResult FlightChanged(FlightDetailUpdateDto flightDetailUpdateDto)
        {
            Console.WriteLine($"Webhook received from: {flightDetailUpdateDto.Publisher}");

            var secretModel =
                _context.SubscriptionSecrets.FirstOrDefault(
                    s => s.Publisher == flightDetailUpdateDto.Publisher 
                         && s.Secret == flightDetailUpdateDto.Secret);

            if (secretModel == null)
            {
                Console.WriteLine("Invalid secret - webhook ignored");
                return BadRequest();
            }

            Console.WriteLine("Valid webhook");
            Console.WriteLine($"Old price: {flightDetailUpdateDto.OldPrice}, New Price: {flightDetailUpdateDto.NewPrice}");
            return NoContent();
        }
    }
}